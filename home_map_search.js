
//<![CDATA[

var gmarkers = [];
var htmls = [];
var to_htmls = [];
var from_htmls = [];
var i=0;
var tooltips = [];
var n=0;
var overlays = [];
var rounds = 0;
var home_ids = [];
var map_is_moving = false;
var first_time = false;
// ===== Start with an empty GLatLngBounds object =====
var bounds;

// Display the map, with some controls and set the initial location
var map;

if (Drupal.jsEnabled) {
	$(document).ready(loadMap);

	document.onkeyup = keyupMode;
}

function keyupMode(e){
	var key = window.event ? event.keyCode : e.which;

	// if the 'r' key is pressed
	if (key == 82) {
		map.closeInfoWindow();
		reloadMarkers();
	}

}
// A function to create the marker and set up the event window
function createMarker(point,html) {
  var marker = new GMarker(point);

  // The info window version with the "to here" form open
  to_htmls[i] = html + '<br>Directions: <b>To here</b> - <a href="javascript:fromhere(' + i + ')">From here</a>' +
     '<br>Start address:<form action="http://maps.google.com/maps" method="get" target="_blank">' +
     '<input type="text" SIZE=40 MAXLENGTH=40 name="saddr" id="saddr" value="" /><br>' +
     '<INPUT value="Get Directions" TYPE="SUBMIT">' +
     '<input type="hidden" name="daddr" value="' + point.lat() + ',' + point.lng() +
     '"/>';
  // The info window version with the "to here" form open
  from_htmls[i] = html + '<br>Directions: <a href="javascript:tohere(' + i + ')">To here</a> - <b>From here</b>' +
     '<br>End address:<form action="http://maps.google.com/maps" method="get"" target="_blank">' +
     '<input type="text" SIZE=40 MAXLENGTH=40 name="daddr" id="daddr" value="" /><br>' +
     '<INPUT value="Get Directions" TYPE="SUBMIT">' +
     '<input type="hidden" name="saddr" value="' + point.lat() + ',' + point.lng() +
     '"/>';
  // The inactive version of the direction info
  html = html + '<br>Directions: <a href="javascript:tohere('+i+')">To here</a> - <a href="javascript:fromhere('+i+')">From here</a>';



  var tooltip = document.createElement("div");
  map.getPane(G_MAP_FLOAT_PANE).appendChild(tooltip);
  //tooltip.style.visibility="hidden";

  GEvent.addListener(marker, "click", function() {
  	//map.removeOverlay(marker);
    marker.openInfoWindowHtml(html);
    //map.addOverlay(new GMarker(point));
  });
  gmarkers[i] = marker;
  htmls[i] = html;
  i++;
  return marker;
}
function createTLabel(point,html,type,home_id) {
	var label = new TLabel();
	label.id = 'Label ' + n + '_' + rounds;
	label.anchorLatLng = point;

	if (type == "home") {
		// this is a home
		label.anchorPoint = 'bottomCenter';
		label.content = '<img src="'+ base_path + module_path +'/gmarker.gif" width=20 height=34 />';
		label.markerOffset = new GSize(-1,-5);
	}

	label.home_id = home_id;
	label.type = type;
	// remove _rounds from here... keep n and just keep incrementing it!
	overlays[n] = label;
	map.addTLabel(label);

	GEvent.addDomListener(document.getElementById(label.id), "click", function() {
		map.closeInfoWindow();
		//if (document.getElementById('add_to_ter').checked) {
		if (label.type == 'home') {
			map.openInfoWindowHtml(point, html, {pixelOffset: new GSize(0,-28)} );
		}
		return;
		// TODO: the rest isn't needed????
		// the rest is to add the clicked marker to an array...

		// restructure the overlays array
		var i, clear = keep = [];
	  //alert('overlays length: '+ overlays.length);
	  for (i=0; i < overlays.length; i++) {
	    if (overlays[i].id == label.id) {
	    	map.removeTLabel(overlays[i]);
	    }
	    else {
	      keep.push(overlays[i]);
	    }
	  }
	  overlays = keep;
	  n=overlays.length;
	  rounds++;

		if (label.type == 'home') {
			// add the home_id to the home_ids array
			home_ids.push(label.home_id);
			createTLabel(point, html, 'added', label.home_id);
		} else {
			// remove this home_id from the home_ids array
			keep = [];
			for (i=0; i < home_ids.length; i++) {
		    if (home_ids[i] != label.home_id) {
		      keep.push(home_ids[i]);
		    }
		  }
		  home_ids = keep;
			createTLabel(point, html, 'home', label.home_id);
		}
		//document.getElementById("selected_persons").innerHTML = home_ids.length;
		//map.removeTLabel(label);
	});

	n++;
}
// functions that open the directions forms
function tohere(i) {
  gmarkers[i].openInfoWindowHtml(to_htmls[i]);
}
function fromhere(i) {
  gmarkers[i].openInfoWindowHtml(from_htmls[i]);
}
function resizeMap(id, newwidth, newheight){
	var e = document.getElementById(id);
	e.style.width = parseInt(newwidth) + 'px';
	e.style.height = parseInt(newheight) + 'px';
}
function loadMap() {
	// Check to see if this browser can run the Google API
	if (!GBrowserIsCompatible()) {
		return;
	}

	// Start code for "Refresh" button in the top right
	function TextualZoomControl() {
	}
	TextualZoomControl.prototype = new GControl();

	TextualZoomControl.prototype.initialize = function(map) {
	  var container = document.createElement("div");

	  var refreshDiv = document.createElement("div");
	  this.setButtonStyle_(refreshDiv);
	  container.appendChild(refreshDiv);
	  refreshDiv.appendChild(document.createTextNode("Refresh"));
	  GEvent.addDomListener(refreshDiv, "click", function() {
	    reloadMarkers();
	  });

	  map.getContainer().appendChild(container);
	  return container;
	}
	TextualZoomControl.prototype.getDefaultPosition = function() {
	  return new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(205, 7));
	}
	TextualZoomControl.prototype.setButtonStyle_ = function(button) {
	  button.style.textDecoration = "none";
	  button.style.color = "#0000cc";
	  button.style.backgroundColor = "white";
	  button.style.font = "small Arial";
	  button.style.border = "1px solid black";
	  button.style.padding = "2px";
	  button.style.marginBottom = "3px";
	  button.style.paddingBottom = "5px";
	  button.style.textAlign = "center";
	  button.style.width = "6em";
	  button.style.cursor = "pointer";
	}
	// End code for "Refresh" button


	map = new GMap2(document.getElementById("search_map"));
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
	map.addControl(new TextualZoomControl());
	map.setCenter(new GLatLng(initial_latitude,initial_longitude), 11);

	//GEvent.addListener(map, "moveend", reloadMarkers);
	GEvent.addListener(map, "zoomend", reloadMarkers);
	// fire the first "move" event
	reloadMarkers();
}
function refreshShownMarkers() {
	// BETA, doesn't work. Try to make it work.
	var the_bounds = map.getBounds();
	var southWest = the_bounds.getSouthWest();
	var northEast = the_bounds.getNorthEast();


	var i, clear = keep = [];

	for (i=0; i < overlays.length; i++) {
		createTLabel(overlays[i].point, document.getElementById(overlays[i].home_id).innerHTML, overlays[i].type, overlays[i].home_id);
		map.removeTLabel(overlays[i]);
	}
	//overlays = keep;
	//n=overlays.length;
	//rounds++;
}
function reloadMarkers() {
	if (map_is_moving) {
		return;
	}

	map_is_moving = true;

	// this is the function that will refresh the markers when the map is moved
	removeOverlays("home");

	if (first_time) {
		bounds = new GLatLngBounds();
	}

	GDownloadUrl(base_path +"?q=search/home/results/xml/autocomplete&"+ getQuery() , function(data, responseCode) {
		var xml = GXml.parse(data);
		var markers = xml.documentElement.getElementsByTagName("marker");
		for (var i = 0; i < markers.length; i++) {
			var point = new GLatLng(parseFloat(markers[i].getAttribute("lat")), parseFloat(markers[i].getAttribute("lng")));
			if (first_time) {
				bounds.extend(point);
			}
			createTLabel(point, markers[i].getAttribute("html"), 'home', markers[i].getAttribute("nid"));
		}
	});

	if (first_time) {
		// ===== determine the zoom level from the bounds =====
		map.setZoom(map.getBoundsZoomLevel(bounds));

		// ===== determine the centre from the bounds ======
		var clat = (bounds.getNorthEast().lat() + bounds.getSouthWest().lat()) / 2;
		var clng = (bounds.getNorthEast().lng() + bounds.getSouthWest().lng()) / 2;

		map.setCenter(new GLatLng(clat,clng));
		first_time = false;

	}
	map_is_moving = false;
}
function removeOverlays(type) {
	var the_bounds = map.getBounds();
	var southWest = the_bounds.getSouthWest();
	var northEast = the_bounds.getNorthEast();
	//return finalQuery +'edit[pids_not]='+ home_ids.join(',') +'&edit[minlat]='+ southWest.lat() + '&edit[maxlat]='+ northEast.lat() +"&edit[minlong]=" + southWest.lng() + "&edit[maxlong]=" + northEast.lng();

	var i, clear = keep = [];
	//alert('overlays length: '+ overlays.length);
	for (i=0; i < overlays.length; i++) {
		if (overlays[i].type == type) {// && overlays[i].anchorLatLng.x < southWest.lng() && overlays[i].anchorLatLng.x > northEast.lng() && overlays[i].anchorLatLng.y > southWest.lat() && overlays[i].anchorLatLng.y < northEast.lat() ) {
			map.removeTLabel(overlays[i]);
		}
		else {
			keep.push(overlays[i]);
		}
	}
	overlays = keep;
	n=overlays.length;
	rounds++;
}
function getQuery() {
	var finalQuery = '';
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if (pair[0] != 'q') {
			finalQuery += vars[i] + '&';
		}
	}
	if (first_time) {
		return finalQuery;
	} else {
		var the_bounds    = map.getBounds();
		var southWest = the_bounds.getSouthWest();
		var northEast = the_bounds.getNorthEast();
		return finalQuery +'nids_not='+ home_ids.join(',') +'&minlat='+ southWest.lat() + '&maxlat='+ northEast.lat() +"&minlong=" + southWest.lng() + "&maxlong=" + northEast.lng();
	}
}
//]]>