<?php
if (!is_numeric($_GET['nid'])) {
	exit;
}

$systempath = realpath('.');
if (strpos($systempath, '/modules/') !== false) {
	// linux
	chdir(substr($systempath, 0, strpos($systempath, '/modules/')));
} else {
	// windows
	chdir(substr($systempath, 0, strpos($systempath, '\\modules\\')));
}

require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

global $base_url;

// fix the http://example.com/modules/home/files/30/03/3/ problem
if (strpos($base_url, '/modules') !== false) {
	$base_url = substr($base_url, 0, strpos($base_url, '/modules'));
}

function home_path($nid) {
	if (!is_numeric($nid)) {
		settype($nid, 'int');
	}
	return substr($nid .'0', 0, 2) .'/'. substr('0'. $nid, -2) .'/'. $nid;
}
if (!$node = db_fetch_object(db_query("SELECT * FROM {home} h INNER JOIN {node} n ON h.hid = n.nid WHERE hid = %d", $_GET['nid']))) {
	exit;
}
$node->images = unserialize($node->images);
$path = $base_url .'/'. variable_get('file_directory_path', 'files') .'/'. home_path($node->nid);
header('Content-type: text/xml');

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<simpleviewerGallery maxImageWidth="700" maxImageHeight="700" textColor="0x404040" frameColor="0x000000" frameWidth="4" stagePadding="0" thumbnailColumns="5" thumbnailRows="1" navPosition="top" title="" enableRightClickOpen="true" backgroundImagePath="" imagePath="<?php echo $path; ?>/" thumbPath="<?php echo $path; ?>/thumb/">
<?php
foreach ($node->images as $photo) {
	echo '<image><filename>preview_'. htmlspecialchars($photo['filename']) .'</filename><caption><![CDATA['. htmlspecialchars($photo['caption']) .']]></caption></image>';
}
?>
</simpleviewerGallery>